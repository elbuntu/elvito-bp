<?php
global $wpdb;

$groups = BP_XProfile_Group::get( array(
    'fetch_fields' => true
));

$updated = "N";

if($_GET['rmid']):
    $id = $_GET['rmid'];
    $wpdb->query("DELETE FROM bb_upgrades WHERE id='$id'"); 
    $updated = "Y";
endif;

if($_POST['formid'] == '4'):
    $price = $_POST['price'];
    $membership = $_POST['membership'];
    $term = $_POST['term'];
    $curr = $_POST['currency'];
    $wpdb->query("INSERT INTO bb_upgrades (price, membership, term, currency) VALUES ('$price', '$membership', '$term', '$curr')");
    $updated = "Y";
endif;

if($_POST['formid'] == '3'):
    $paypal_email = $_POST['paypal'];
    $wpdb->query("UPDATE bb_profiles_settings SET option_value='$paypal_email' WHERE option_name='paypal'");
    $updated = "Y";
endif;

?>

<?php if($updated == "Y"): ?>
<div id="message" class="updated" style="margin-left: 0px; width: 93.5%;">
	<p><strong>The changes are made :)</strong></p>
</div>
<?php endif; ?>
<div id="message" class="updated" style="margin-left: 0px; width: 93.5%;">
	<p><strong>To embed the subscriptions table into the subscriptions page use the shortcode [elvitosub]</strong></p>
</div>
<?php $paypal = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM bb_profiles_settings WHERE option_name = 'paypal';")); ?>

<h2>Subscriptions</h2>
<form method="post" action="#" name="pr_form3">
<table class="widefat page" cellspacing="0" style="width:95%">
<thead>  
<tr>
<th>
PayPal Info
</th>
</tr>
</thead> 
<tr>
<td>
E-Mail (for Payment): <input type="text" value="<?php echo $paypal; ?>" name="paypal" style="width: 300px;"> </td>
</tr>
<tr>
<td><input type="submit" class="button" value="Update" style="float: right;" /><input type="hidden" name="formid" value="3" /></td>
</tr>
</table>
</form>
<h2>Paid Memberships</h2>
<table class="widefat page" cellspacing="0" style="width:95%; margin-bottom: 10px;">
<thead>  
<tr>
<th>
Price
</th>
<th>
Membership Level
</th>
<th>
Term
</th>
<th>
Currency Code
</th>
<th>
</th>
</tr>
</thead>
<?php 
$get_items = $wpdb->get_results("SELECT * FROM bb_upgrades");
foreach ($get_items as $item) {
?>
<form method="post" name="formid4">
<tr>
<td>
<?php echo $item->price; ?>
</td>
<td valign="middle">
<?php echo $item->membership; ?>
</td>
<td>
<?php echo $item->term; ?>
</td>
<td>
<?php echo $item->currency; ?>
</td>
<td>
<input type="button" onclick="window.location.href='?page=elvito-subscriptions&rmid=<?php echo $item->id; ?>'" class="button" value="Remove" style="float: right;" />
</td>
</tr>
</form>
<?php } ?>
<form method="post" action="#" name="pr_form4">
<tr>
<th>
<input type="text" value="" name="price" />
</th>
<th valign="middle">
<select name="membership">
<?php wp_dropdown_roles(); ?>
</select>
</th>
<th>
    <select name="term">
        <option>One-Time Payment</option>
        <option>Monthly</option>
        <option>Yearly</option>
    </select>
</th>
<th>
    <select name="currency">
        <option>EUR</option>
        <option>USD</option>
        <option>GBP</option>
    </select>
</th>
<td>
<input type="hidden" name="formid" value="4" />
<input type="submit" class="button" style="margin-top: 6px; float: right;" value="+ Add Option" />
</td>
</tr>
</table>
</form>

<?php
global $wpdb;

$groups = BP_XProfile_Group::get( array(
    'fetch_fields' => true
));

$updated = "N";

if($_GET['rmid']):
    $id = $_GET['rmid'];
    $wpdb->query("DELETE FROM bb_profiles WHERE id='$id'"); 
    $updated = "Y";
endif;

if($_GET['update']):
    $rndid = $_GET['update'];
    $new_pub = $_POST['public'];
    $new_mem = $_POST['member'];
    $wpdb->query("UPDATE bb_profiles SET member_hidden='$new_mem', public_hidden='$new_pub' WHERE id='$rndid'");
    $updated = "Y";
endif;

if($_POST['formid'] == "1"):
    $level = $_POST['level'];
    $group = $_POST['group'];
    $pub = $_POST['public'];
    $member = $_POST['member'];
    $wpdb->query("INSERT INTO bb_profiles (membership_level, bb_group, member_hidden, public_hidden) VALUES ('$level', '$group', '$member', '$pub')");
    $updated = "Y";
endif;

if($_POST['formid'] == "2"):
    $msg = $_POST['message'];
    $btn_en = $_POST['button_en'];
    $btn_link = $_POST['button_link'];
    $btn_text = $_POST['button_text'];
    $wpdb->query("UPDATE bb_profiles_settings SET option_value='$msg' WHERE option_name='message'");
    $wpdb->query("UPDATE bb_profiles_settings SET option_value='$btn_en' WHERE option_name='button_en'");
    $wpdb->query("UPDATE bb_profiles_settings SET option_value='$btn_text' WHERE option_name='button_text'");
    $wpdb->query("UPDATE bb_profiles_settings SET option_value='$btn_link' WHERE option_name='button_link'");
    $updated = "Y";
endif;

?>

<?php if($updated == "Y"): ?>
<div id="message" class="updated" style="margin-left: 0px; width: 93.5%;">
	<p><strong>The changes are made :)</strong></p>
</div>
<?php endif; ?>

<h2>Profile Group Administration</h2>
<table class="widefat page" cellspacing="0" style="width:95%; margin-bottom: 10px;">
<thead>  
<tr>
<th>
Membership Level
</th>
<th>
BuddyPress Group
</th>
<th>
Visibility (Member)
</th>
<th>
Visibility (Public)
</th>
<th>
</th>
</tr>
</thead>
<?php 
$get_items = $wpdb->get_results("SELECT * FROM bb_profiles");
$i = '0';
foreach ($get_items as $item) {
?>
<form method="post" action="?page=elvito-profiles&update=<?php echo $item->id; ?>" name="update">
<tr>
<td>
<?php echo $item->membership_level; ?>
</td>
<td>
<?php echo $item->bb_group; ?>
</td>
<td>
<select name="member">
<option value="<?php echo $item->member_hidden; ?>"><?php echo $item->member_hidden; ?></option>
<option value="hidden">Hidden</option>
<option value="shown">Shown</option>
</select>
</td>
<td>
<select name="public">
<option value="<?php echo $item->public_hidden; ?>"><?php echo $item->public_hidden; ?></option>
<option value="hidden">Hidden</option>
<option value="shown">Shown</option>
</select>
</td>
<td>
<input type="button" onclick="window.location.href='?page=elvito-profiles&rmid=<?php echo $item->id; ?>'" class="button" value="Remove" style="float: right;" />
<input type="submit" class="button" value="Update" style="float: right;" />
</td>
</tr>
</form>
<?php } ?>

<form method="post" action="#" name="pr_form2">
<tr>
<th valign="middle">
<select name="level">
<?php wp_dropdown_roles(); ?>
</select>
</th>
<th valign="middle">
<select name="group">
<?php
for ( $i = 0; $i < count($groups); $i++ ) { // TODO: foreach
echo "<option value='" . sanitize_title( $groups[$i]->name ) . "'>";
echo $groups[$i]->name;
echo "</option>";
}
?>

</select>
<input type="hidden" name="formid" value="1" />
</th>
<th valign="middle" align="center">
<select name="member">
<option value="hidden">Hidden</option>
<option value="shown">Shown</option>
</select>
</th>
<th valign="middle" align="center">
<select name="public">
<option value="hidden">Hidden</option>
<option value="shown">Shown</option>
</select>
</th>
<td>
<input type="submit" class="button" style="margin-top: 6px; float: right;" value="+ Add To Rules" />
<input type="hidden" value="1" name="formid" />
</td>
</tr>
</form>
</table>


<form method="post" action="#" name="pr_form2">
<?php 
$message = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM bb_profiles_settings WHERE option_name = 'message';"));
$btn_en = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM bb_profiles_settings WHERE option_name = 'button_en';"));
$btn_text = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM bb_profiles_settings WHERE option_name = 'button_text';"));
$btn_link = $wpdb->get_var($wpdb->prepare("SELECT option_value FROM bb_profiles_settings WHERE option_name = 'button_link';"));
?>
<table class="widefat page" cellspacing="0" style="width:95%">
<thead>  
<tr>
<th>
Advanced Options
</th>
</tr>
</thead> 
<tr>
<td style="background: #eee;"><input type="checkbox" <?php if ($btn_en == 'on') { echo 'checked'; } ?> name="button_en"> <i>Enable Upgrade Nav (Adds a nav item to profile edit menu that links to your membership upgrade page)</i></td>
</tr>
<tr>
<td>
Nav Text : <input type="text" value="<?php echo $btn_text; ?>" name="button_text" style="width: 300px;"> </td>
</tr>
<tr>
<td>
Nav Link : <input type="text" value="<?php echo $btn_link; ?>" name="button_link" style="width: 300px;"> </td>
</tr>
<tr>
<tr>
<td style="background: #eee;"><i>Message (for fields that the user can see but won't be published)</i></td>
</tr>
<tr>
<td><textarea style="width: 100%; height: 200px;" name="message"><?php echo $message; ?></textarea></td>
</tr>
<tr>
<td><input type="submit" class="button" value="Update Options" style="float: right;" /><input type="hidden" name="formid" value="2" /></td>
</tr>
</table>
</form>
<?php
global $wpdb;
if($_POST['serial']):
    $serial = $_POST['serial'];
    $wpdb->query("UPDATE bb_profiles_settings SET option_value='$serial' WHERE option_name='serial'");
endif;
?>
<table class="widefat page" cellspacing="0" style="width:95%; margin-top: 10px; margin-bottom: 10px;">
<thead>  
<tr>
<th colspan="2">
<strong>Elvito Members (Buddy Press)</strong>
</th>
</tr>
</thead> 
<tr>
<td>
    <p>
        Author: Ashley Johnson (Elvito Solutions)<br>
        Version: 1.3<br>
        Website: http://elvito-solutions.com
    </p>
</td>
<td width="320">
    <p>
        This plugin is a tool to block certain profile fields from non-paying members, this tool can be used in many ways and has a feature to allow payment through PayPal.
    </p>
</td>
</tr>
</table>

<table class="widefat page" cellspacing="0" style="width:95%; margin-top: 10px; margin-bottom: 10px;">
<thead>  
<tr>
<th>
<strong>Donate to Elvito BP</strong>
</th>
<th>
<strong>Need Help/Support?</strong>
</th>
</tr>
</thead> 
<tr>
<td width="50%">
    <p>
       Although this plugin is free to use and distribute donations enable us
       to develop new features and fix bugs. if you like this plugin we kindly 
       ask that you donate using the form below:
    </p>
<h2>Donation Form</h2>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_donations">
<input type="hidden" name="business" value="hallo@elgunvo.de">
<input type="hidden" name="lc" value="US">
<input type="hidden" name="item_name" value="Elvito WP Plugins">
<input type="hidden" name="item_number" value="109998">
Amount: <select name="amount">
    <option value="2.50">€2,50</option>
    <option value="5.00">€5</option>
    <option value="10.00">€10</option>
    <option value="15.00">€15</option>
</select>
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="no_note" value="0">
<input type="hidden" name="currency_code" value="EUR">
<input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
<input type="submit" value="Donate Via Paypal" />
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
<p>&nbsp;</p>
</td>
<td>
    <p>For support information please visit the plugin page at http://elvito-solutions.com/buddypress</p>
</td>
</tr>
</table>
<?php
/*
 Plugin Name: Elvito BP
 Plugin URI: http://elvito-solutions.com
 Description: Tools for BuddyPress
 Author: Ashley Johnson
 Author URI: http://www.elvito-solutions.com
 License: GNU GENERAL PUBLIC LICENSE 3.0 http://www.gnu.org/licenses/gpl.txt
 Version: 1.33
 Text-Domain: bp-elvito
 Site Wide Only: true
*/

/********************************
 * Activate the plugin for use! *
 *******************************/

function bp_elvito_activate() {
    
    global $wpdb;
    
    if(!empty($wpdb->charset)):
        $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
    endif;
	
    $wpdb->query("CREATE TABLE bb_profiles (
                            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                            membership_level text, 
                            bb_group text, 
                            member_hidden text, 
                            public_hidden text) {$charset_collate};");
                            



    $wpdb->query("CREATE TABLE bb_profiles_settings (
                            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                            option_name text,
                            option_value text) {$charset_collate};");
                            
    $wpdb->query("CREATE TABLE bb_banner_groups (
                            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                            group_name text,
                            group_desc text) {$charset_collate};");
    
    $wpdb->query("CREATE TABLE bb_banner (
                            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                            banner_html text,
                            banner_group text) {$charset_collate};");

    $wpdb->query("CREATE TABLE bb_upgrades (
                            id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                            price text,
                            term text,
                            currency text,
                            membership text) {$charset_collate};");
   
    $wpdb->query("INSERT INTO bb_profiles_settings (option_name, option_value) VALUES ('button_en', '')");
    $wpdb->query("INSERT INTO bb_profiles_settings (option_name, option_value) VALUES ('button_text', 'sometext')");
    $wpdb->query("INSERT INTO bb_profiles_settings (option_name, option_value) VALUES ('button_link', 'http://your.com/page')");
    $wpdb->query("INSERT INTO bb_profiles_settings (option_name, option_value) VALUES ('version', '1.22')");
    $wpdb->query("INSERT INTO bb_profiles_settings (option_name, option_value) VALUES ('paypal', 'sample@yourdomain.com')");
    $wpdb->query("INSERT INTO bb_profiles_settings (option_name, option_value) VALUES ('serial', '')");
    $wpdb->query("INSERT INTO bb_profiles_settings (option_name, option_value) VALUES ('message', '')");
    

                            
}

register_activation_hook( __FILE__, 'bp_elvito_activate' );

//include essential code
require( dirname( __FILE__ ) . '/frontend.php' );

/*******************************************************************
 * Elvito Main Menu (Builds the menu for the administration panel) *
 *******************************************************************/

function bp_elvito_admin_menu() {
	global $bp;
	
	if(!is_super_admin()):
            return false;
        endif;
	
	require ( dirname( __FILE__ ) . '/admin/admin.php' );
	
        add_menu_page('Elvito BP Splash', 'Elvito BP', 'manage_options', 'elvito-information', 'splash_function');
        add_submenu_page( 'elvito-information', '', '', 'manage_options', 'elvito-information', 'splash_function');
        add_submenu_page( 'elvito-information', 'Profile Manager', 'Profile Manager', 'manage_options', 'elvito-profiles', 'profiles_function');
        add_submenu_page( 'elvito-information', 'Subscriptions', 'Subscriptions', 'manage_options', 'elvito-subscriptions', 'subscriptions_function');
        add_submenu_page( 'elvito-information', 'Banner Rotator', 'Bannner Rotator', 'manage_options', 'elvito-rotator', 'banner_function');
	
}

add_action( 'admin_menu', 'bp_elvito_admin_menu' );

function bp_elvito_deactivate() {
    global $wpdb;
    $wpdb->query("DROP TABLE bb_profiles");
    $wpdb->query("DROP TABLE bb_profiles_settings");
    $wpdb->query("DROP TABLE bb_upgrades");
}

register_deactivation_hook( __FILE__, 'bp_elvito_deactivate');

?>
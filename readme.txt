=== Plugin Name ===
Contributors: elvito-solutions
Tags: Buddypress, Profile, Social Network
Requires at least: 2.8
Tested up to: 3.4
Stable tag: 1.33

Elvito BP contains features that help you to monetize your blog by allowing you to hide certain fields from non-paying members.

== Description ==

Elvito BP contains features that help you to monetize your blog by allowing you to hide certain fields from non-paying members.

Features:

* Hide Profile Fields from Non-Paying Members
* Simple to use interface
* Banner Rotator in Posts for affiliate banners. BETA
* Create subscriptions with paypal intergration
* Teaser profiles (where the user can fill out the information but its not viewable to other users)
* Create Nav Link for subscription page where the message is displayed.

== Installation ==

Before you activate this plugin make sure that you have Buddypress v1.2.7 or higher.

1. Upload the buddypress-profiles folder to plugins folder.
2. Go to your buddy-press template and search for edit.php under members/single/profile.
3. Change bp_profile_group_tabs(); with  bp_pm_group_tabs();
4. Activate the BP-Profiles Manager in wp-admin under plugins.
5. Go to settings -> BP-Profiles Manager
6. Your Good to go!

== Removing the plugin ==

All should be fine should you want to deactivate this plugin however do make sure that you replace bp_pm_group_tabs(); with bp_profile_group_tabs(); (reverse to install info number 3) otherwise the profile edit tabs will dissapear.

== Frequently Asked Questions ==

-- Where is edit.php? --

In the buddy-press template it should be located at members/single/profile/edit.php


-- Other Information --

This is a free version and you can create 3 rules. For unlimited rules in this and future updates
including support please donate using the form inside the plugins admin page.

If you are having difficulties getting this plugin to work please email me with the details 
at ashley.johnson@elvito-solutions.com or comment on the plugin page.

== Changelog ==

= 1.33 =

* Made amendments to subscription
* Created a banner rotator for posts BETA

= 1.3 =

* Removed lock on unlimited use and instead ask kindly for a small donation to keep the plugin maintained
* Create subscriptions and then list them on your upgrade page by using a shortcode.
* Recoded.
* Fixed message not saving problem on the profiles manager page.

= 1.22 =

* Taken over by elvito-solutions 

= 1.3 =
* Fixed bp_head function for higher versions of wordpress

= 1.2 =
* Cleaned the code where older code was not in use.
* Fixed mysql error within the profile hider.

= 1.1.1 =
* Fixed Broken Images and re-wrote the upgrade script.

= 1.1 =
* Added an option to create a navigation item that links to your profile upgrades page.
* re-wrote 1 database
* Wrote a database update script.
* Back end modifications

= 1.0 =
* Written Initial Version
* Programmed backend
* Developed Front End

<?php code(); // goes in backticks ?>